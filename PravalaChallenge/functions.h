#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <winsock2.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

string IntToString (int a)
{
    ostringstream temp;
    temp<<a;
    return temp.str();
}

void ParseUrl(const char *mUrl, string &serverName, string &filepath, string &filename)
{
    string::size_type n;
    string url = mUrl;

    if (url.substr(0,7) == "http://")
        url.erase(0,7);

    if (url.substr(0,8) == "https://")
        url.erase(0,8);

    n = url.find('/');
    if (n != string::npos)
    {
        serverName = url.substr(0,n);
        filepath = url.substr(n);
        n = filepath.rfind('/');
        filename = filepath.substr(n+1);
    }

    else
    {
        serverName = url;
        filepath = "/";
        filename = "";
    }
}

int connecttoServer(SOCKET &ConnectSocket,  SOCKADDR_IN *SockAddr ){

    int iResult;
    iResult = connect(ConnectSocket,(sockaddr *)SockAddr,sizeof(*SockAddr));

    return iResult;
}

int sendtoServer(SOCKET &ConnectSocket, const char * message){

    int iResult;
    iResult = send(ConnectSocket, message, strlen(message),0 );

    return iResult;
}

//opens socket and connects to server
int setupTCPConnection(SOCKET &ConnectSocket, SOCKADDR_IN* SockAddr){

    int iResult;
    ConnectSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
     if (ConnectSocket == INVALID_SOCKET){
        cout<<"invalid socket";
        WSACleanup();
        return -1;

     }

     //connect to server
    iResult = connecttoServer(ConnectSocket, SockAddr);
    if(iResult == SOCKET_ERROR){
        cout << "Could not connect";
        iResult = closesocket(ConnectSocket);
        WSACleanup();
        return -1;

    }

    return 0;

}

#endif // FUNCTIONS_H
