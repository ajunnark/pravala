#include <iostream>
#include <winsock2.h>
#include <string>
#include <fstream>
#include <sstream>
#include "functions.h"

//#pragma comment(lib, "Ws2_32.lib")
#define DEFAULT_BUFLEN 10000

using namespace std;

int main(int argc, char *argv[])
{

    if(argc != 2){
        cout<<"Usage : "+ string(argv[0])+" [url]"<<endl;
        return -1;
    }

    WSADATA wsaData;
    hostent *host;

    string serverName, filepath, filename;


    int iResult;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        cout<<"WSAStartup failed: "<<iResult;
        return -1;
    }
    string url = argv[1];
    unsigned long addr;

    //parse url to get filename, servername and filepath
    ParseUrl(url.c_str(), serverName, filepath, filename);

    //get host address to connect to TCP socket
    if(inet_addr((char*)serverName.c_str())==INADDR_NONE)
    {
        host=gethostbyname((char*)serverName.c_str());
    }
    else
    {
        addr=inet_addr((char*)serverName.c_str());
        host=gethostbyaddr((char*)&addr,sizeof(addr),AF_INET);
    }

    if(host==NULL){
        cout<<"Server cannot be found"<<endl;
        return -1;
    }



    SOCKET ConnectSocket;


    SOCKADDR_IN SockAddr;
    SockAddr.sin_port=htons(80); //default for HTTP is Port 80
    SockAddr.sin_family=AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

    //open socket and connect to server
    iResult = setupTCPConnection(ConnectSocket, &SockAddr);
    if(iResult == -1){
        return -1;
    }

    //issue HEAD request to parse filesize
    string head_http = "HEAD "+ filepath + " HTTP/1.1\r\nHost: " + serverName + "\r\nConnection: close\r\n\r\n";
    cout<<endl<<head_http;

    //issue HEAD request first to get filesize
    iResult = sendtoServer(ConnectSocket, head_http.c_str());
    if (iResult == SOCKET_ERROR) {

        cout<<"Sending failed"<<endl;
        closesocket(ConnectSocket);
        WSACleanup();
        return -1;
    }
    cout<<endl;


    int recvbuflen = DEFAULT_BUFLEN;
    char recvbuf[DEFAULT_BUFLEN] = "";

    int lengthfound = 0, length, i = 0;
    char* parsedL;
    string filesize;
    ofstream myfile;

    if(filename != ""){
        myfile.open(filename.c_str(), ios::binary);

    }

    do {

        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        if ( iResult < 0 ){
            cout<<"Error receiving data from Server"<<endl;
            return -1;

        } else if ( iResult == 0 )
            wprintf(L"Connection closed\n");

        else if(lengthfound == 0){
            parsedL = strstr(recvbuf, "Content-Length: ");
            if(parsedL != NULL){
                parsedL +=16;

                for(length = 0; *parsedL != 0 && *parsedL != '\n'; ++parsedL)
                    length++;

                parsedL -= length;
                lengthfound= 1;

                while(i < length){
                    filesize+= *parsedL;
                    ++i;
                    parsedL++;
                }
                cout<<"FileSize: "<<filesize<<endl;
                break;

            }
        }

    } while( iResult > 0 );

    //check if file size is parsed
    if(!lengthfound || !atoi(filesize.c_str())){
        cout<<"Error in getting file size from server";
        WSACleanup();
        return -1;
    }

    int fileSize = atoi(filesize.c_str());

    //if file size is greater than 4 MiB, request file in 4 chunks of 1MiB each
    if(fileSize > 4*1048576 ){
        i=0;
        int j = 1048575, k=1;
        string get_http = "";
        do{

            iResult = setupTCPConnection(ConnectSocket, &SockAddr);
            if(iResult == -1){
                return -1;
            }

            get_http = "GET "+ filepath + " HTTP/1.1\r\nHost: " + serverName + "\r\nRange: bytes="+ IntToString(i)+"-"+ IntToString(j)+"\r\nConnection: close\r\n\r\n";

            //send GET request to server
            iResult = sendtoServer(ConnectSocket, get_http.c_str());
            if (iResult == SOCKET_ERROR) {

                cout<<"Sending failed"<<endl;
                closesocket(ConnectSocket);
                WSACleanup();
                return -1;
            }

            int isnheader = 0, endOfHeaderFound = 0;
            char* parsed = NULL;
            int index = 0;

            cout<<"Downloading chunk "<<k<<".."<<endl;
            do {

                iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
                if ( iResult < 0 ){
                    cout<<"Error receiving data from Server"<<endl;
                    return -1;
                } else if ( iResult == 0 ){
                    wprintf(L"Connection closed\n");
                }

                //parse HTTP response and only write the contents of the file

                //check if request timed out
                if(strstr(recvbuf,"408 Request Time-out")!= NULL){
                    cout<<"Request timed out"<<endl;
                    closesocket(ConnectSocket);
                    WSACleanup();
                    return -1;
                }

                if(isnheader == 0){
                    parsed = strstr(recvbuf, "\r\n\r\n");
                    if(parsed != NULL){
                        index = string(recvbuf).find("\r\n\r\n");
                        isnheader = 1;
                    }
                }

                if(isnheader && !endOfHeaderFound) {
                    if(filename !=""){
                        myfile.write((parsed+4),iResult - index);
                        endOfHeaderFound = 1;
                        continue;
                    }
                }

                if(endOfHeaderFound){
                    myfile.write(recvbuf, iResult);
                }

                memset(&recvbuf[0], 0, sizeof(recvbuf));

            } while( iResult > 0 );

            i+=1024*1024;
            j+=1024*1024;
            k++;

        }while(j<=4194303);

    //if file size is smaller than 4MiB, request in 2 chunks
    } else {

        i=0;
        int j = fileSize/2 -1, k=1;
        string get_http = "";
        do{

            iResult = setupTCPConnection(ConnectSocket, &SockAddr);
            if(iResult == -1){
                return -1;
            }

            get_http = "GET "+ filepath + " HTTP/1.1\r\nHost: " + serverName + "\r\nRange: bytes="+ IntToString(i)+"-"+ IntToString(j)+"\r\nConnection: close\r\n\r\n";
            cout<<endl<<get_http;

            iResult = sendtoServer(ConnectSocket, get_http.c_str());
            if (iResult == SOCKET_ERROR) {

                cout<<"Sending failed"<<endl;
                closesocket(ConnectSocket);
                WSACleanup();
                return -1;
            }
            int isnheader = 0, endOfHeaderFound = 0;
            char* parsed = NULL;
            int index;

            cout<<"Downloading chunk "<<k<<".."<<endl;
            do {

                iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
                if ( iResult < 0 ){
                    cout<<"Error receiving data from Server"<<endl;
                    closesocket(ConnectSocket);
                    WSACleanup();
                    return -1;
                }

                if ( iResult == 0 )
                    wprintf(L"Connection closed\n");

                    //parse HTTP response and only write the contents of the file

                    //check if request timed out
                    if(strstr(recvbuf,"408 Request Time-out")!= NULL){
                        cout<<"Request timed out"<<endl;
                        closesocket(ConnectSocket);
                        WSACleanup();
                        return -1;
                    }

                    if(isnheader == 0){
                        parsed = strstr(recvbuf, "\r\n\r\n");
                        if(parsed != NULL){
                            index = string(recvbuf).find("\r\n\r\n");

                            isnheader = 1;
                        }
                    }

                    if(isnheader && !endOfHeaderFound) {
                        if(filename !=""){

                            myfile.write((parsed+4), iResult - index);
                            endOfHeaderFound = 1;
                            continue;
                        }
                    }

                    if(endOfHeaderFound){
                        myfile.write(recvbuf, iResult);
                    }

            } while( iResult > 0 );

            i+= fileSize/2;
            j+= fileSize/2;
            k++;

        }while(j<=fileSize-1);

    }

    closesocket(ConnectSocket);
    WSACleanup();
    return 0;
}
